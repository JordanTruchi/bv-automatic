export default function ({ store, redirect, route }) {
  const userIsLoggedIn = !!store.state.auth.user;
  const regex1 = RegExp('/aponim/admin');
  const regex2 = RegExp('/aponim/sign-in');
  const urlRequiresAuth = regex1.test(route.fullPath);
  const urlRequiresNonAuth = regex2.test(route.fullPath);

  if (!userIsLoggedIn && urlRequiresAuth) {
    return redirect('/aponim/sign-in');
  }
  if (userIsLoggedIn && urlRequiresNonAuth) {
    return redirect('/aponim/admin');
  }
  return Promise.resolve();
}
