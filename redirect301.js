module.exports = function (req, res, next) {
  const host = req.headers.host;
  const url = req.url;
  const canonicalDomain = process.env.NODE_ENV;
  if (host !== canonicalDomain) {
    res.writeHead(301, { Location: 'https://' + canonicalDomain + url });
    return res.end();
  }
  return next();
};
