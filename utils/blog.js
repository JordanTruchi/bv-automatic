export function substrString (string, url) {
  return (
    string.substr(0, 255) +
    "... <a target='_blank' href='https://dopa-tech.fr/jobs/" +
    url +
    "'>Voir l'annonce</a>"
  );
}
