const axios = require('axios');

module.exports = async function getAppRoutes () {
  // Initiate axios
  const instance = axios.create({
    baseURL: `http://${process.env.HOST || 'localhost'}:${
      process.env.PORT || 3000
    }`
  });
  // Fetch languages and store their codes (fr, en, ...) in array
  const { data } = await instance.get('/api/sitemap/dynamic');
  const trueRoute = [];
  for (let i = 0; i < data.blog.length; i++) {
    trueRoute.push(`/blog/${data.blog[i].url}`);
  }
  for (let i = 0; i < data.jobs.length; i++) {
    trueRoute.push(`/jobs/${data.jobs[i].url}`);
  }
  return trueRoute;
};
