export function transformDateForBlog (date) {
  var currentDate = new Date(date);
  var day = currentDate.getDate();
  var month = currentDate.getMonth();
  let trueM = pad(month + 1);
  if (trueM[0] === '0') trueM = trueM[1];
  var year = currentDate.getFullYear();
  let endDay = '';
  endDay =
    currentDate.getDay() - 1 === -1
      ? daysOfWeek.length - 1
      : currentDate.getDay() - 1;
  return (
    daysOfWeek[endDay] + ' ' + day + ', ' + monthNames[trueM - 1] + ' ' + year
  );
}

function pad (n) {
  return n < 10 ? '0' + n : n;
}

const monthNames = [
  'Janvier',
  'Fevrier',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Aout',
  'Septembre',
  'Octobre',
  'Novembre',
  'Décembre'
];

var daysOfWeek = ['Lund', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
