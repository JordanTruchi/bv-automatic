module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'plugin:vue/recommended',
    'standard'
  ],
  rules: {
    semi: [2, 'always'],
    'no-tabs': ['error', { allowIndentationTabs: true }],
    'vue/require-prop-types': 0,
    'vue/no-v-html': 0,
    'no-useless-escape': 0,
    'no-prototype-builtins': 0
  },
  plugins: [
    'vue'
  ]
};
