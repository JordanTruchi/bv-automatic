const getTotalPrice = (cart) => cart.items.reduce((acc, { price, quantity }) => acc + (price * quantity), 0);

const dataCartTest = {
  totalPrice: Number,
  items: [
    { id_product: 1, name: 'huile', price: 10.29, quantity: 2 },
    { id_product: 2, name: 'Moteur', price: 20.18, quantity: 1 }
  ]
};

dataCartTest.totalPrice = getTotalPrice(dataCartTest);

export { dataCartTest };
