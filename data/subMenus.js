const subMenuHuile = {
  mostUsed: {
    label: 'Huiles les plus achétées',
    styleSpecial: true,
    items: [
      { url: '/', name: 'Huile 0w30' },
      { url: '/', name: 'Huile 0w40' },
      { url: '/', name: 'Huile 5w30' },
      { url: '/', name: 'Huile 5w40' },
      { url: '/', name: 'Huile 10w60' }
    ]
  },
  subMenu: {
    items: [
      { url: '/', name: 'Huile Moteur voiture' },
      { url: '/', name: 'Huile moteur selon modèle de voiture' },
      { url: '/', name: 'Huile Moteur Voiture par viscosité' }
    ]
  },
  itemCarMark: {
    label: 'Huiles par marque de voiture',
    items: [
      { url: '/', name: 'Huile pour BMW' },
      { url: '/', name: 'Huiles pour Audi VW Seat' },
      { url: '/', name: 'Huiles pour Mercedes' },
      { url: '/', name: 'Huiles pour Porsche' },
      { url: '/', name: 'Huiles Fiat et Alfa Roméo' }
    ]
  },
  itemMark: {
    label: 'Marques d\'huile',
    items: [
      { url: '/', name: 'BMW' },
      { url: '/', name: 'Castrol' },
      { url: '/', name: 'Fuchs' },
      { url: '/', name: 'Mercedes' },
      { url: '/', name: 'Mobil' }
    ]
  }
};

export const menusTop = {
  itemsHover: { huile: false },
  items: [
    { label: 'Accueil', url: '/', subMenu: false, subMenuItems: [], hoverPropery: '', classes: ['linkItem'] },
    { label: 'Huile', url: '/huile', subMenu: true, subMenuItems: subMenuHuile, hoverPropery: 'huile', classes: ['linkItem'] },
    { label: 'Contact', url: '/contact', subMenu: false, subMenuItems: [], hoverPropery: '', classes: ['contactItem'] }
  ]
};
