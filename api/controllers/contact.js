const sgMail = require('@sendgrid/mail');
const { verifyFormContactData } = require('../utils/contact');

const APIKEY =
  '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';
module.exports = {
  sendMail: (req, res, next) => {
    const { name, email, subject, message } = req.body;

    // Vérifie les champs du formulaire de contact.
    const verifydata = verifyFormContactData(name, email, subject, message);

    if (verifydata) {
      sgMail.setApiKey(APIKEY);
      const msg = {
        to: 'dev.DopaTech@gmail.com',
        from: 'contact.DopaTech@gmail.com',
        subject: subject,
        html: `Mail: ${email}<br>Nom: ${name}<br>Message:<br>${message}`
      };
      sgMail
        .send(msg)
        .then(() => {
          res.json({
            type: 'success',
            message: 'Votre message a bien été envoyé'
          });
        })
        .catch((error) => {
          console.error(error.toString());
          const { message, code, response } = error;
          const { headers, body } = response;
          console.error(headers, message, code);
          res.status(500).json({ type: 'error', message: body });
        });
    }
  }
};
