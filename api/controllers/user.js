const database = require('../utils/database');
const { jwtToken } = require('../utils/jwt');
const db = database.conn();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
  login: (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    if (!email || !password) {
      return res.status(400).json({
        type: 'error',
        message: 'email and password fields are essential for authentication.'
      });
    }
    const sql = 'select * from `users` where `email`= ?';
    db.query(sql, email, (err, results, field) => {
      if (err) {
        return res
          .status(500)
          .json({ type: 'error', message: 'db error', err });
      }
      if (results.length === 0) {
        return res.status(403).json({
          type: 'error',
          message: 'User with provided email not found in database.'
        });
      }
      const user = results[0];
      bcrypt.compare(password, user.password, (error, result) => {
        if (error) {
          return res
            .status(500)
            .json({ type: 'error', message: 'bcrypt error', error });
        }
        if (result) {
          // check time
          const now = Math.floor(Date.now() / 1000);
          const expiresIn = 43200;
          const expireDate = now + expiresIn;
          jwt.sign(
            { id: user.id_user, email: user.email },
            jwtToken(),
            { algorithm: 'HS256', expiresIn: expireDate },
            (err, token) => {
              if (err) {
                console.error(err);
                throw err;
              }
              res.json({
                type: 'success',
                message: 'User logged in.',
                user: { id: user.id_user, email: user.email },
                token: token
              });
            }
          );
        } else {
          res
            .status(403)
            .json({ type: 'error', message: 'Password is incorrect.' });
        }
      });
    });
  },
  verify: (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
      return res
        .status(400)
        .json({ type: 'error', message: 'x-access-token header not found.' });
    }
    jwt.verify(token, jwtToken(), (error, result) => {
      if (error) {
        return res.status(403).json({
          type: 'error',
          message: 'Provided token is invalid.',
          error
        });
      }
      res.json({
        type: 'success',
        message: 'Provided token is valid.',
        result: result
      });
    });
  },
  create: (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    if (!email || !password) {
      return res.status(400).json({
        type: 'error',
        message: 'email and password fields are essential for sign up.'
      });
    }
    const sql = 'select * from `users` where `email`= ?';
    db.query(sql, email, (err, results, field) => {
      if (err) {
        return res.status(500).json({
          type: 'error',
          message: 'La base de données a un problème',
          err
        });
      }
      if (results.length > 0) {
        return res
          .status(403)
          .json({ type: 'error', message: 'User already exist' });
      }
      bcrypt.hash(password, 10, function (err, hash) {
        if (err) {
          return res.status(500).json({
            type: 'error',
            message: "Nous n'avons pas pu crypter votre mot de passe",
            err
          });
        }
        const sql = 'INSERT INTO users (email, password) VALUES (?, ?)';
        db.query(sql, [email, hash], (err, results, field) => {
          if (err) {
            return res.status(500).json({
              type: 'error',
              message: 'La base de données a un problème',
              err
            });
          }
          res.json({
            type: 'success',
            message: 'User created.',
            result: { id: results.insertId, email: email }
          });
        });
      });
    });
  }
};
