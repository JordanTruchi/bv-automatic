const database = require('../utils/database');
const db = database.conn();

module.exports = {
  list: (req, res, next) => {
    const sql = 'SELECT url FROM articles';
    const allUrl = {};
    db.query(sql, (err, result, field) => {
      if (err) {
        return res
          .status(500)
          .json({ type: 'error', message: 'db error', err });
      }
      if (result.length === 0) {
        return res
          .status(400)
          .json({ type: 'error', message: "Aucune url n'a été trouvée..." });
      }
      allUrl.blog = result;
      const sqlJ = 'SELECT url FROM jobs';
      db.query(sqlJ, (err, result, field) => {
        if (err) {
          return res
            .status(500)
            .json({ type: 'error', message: 'db error', err });
        }
        if (result.length === 0) {
          return res
            .status(400)
            .json({ type: 'error', message: "Aucune url n'a été trouvée..." });
        }
        allUrl.jobs = result;
        res.json(allUrl);
      });
    });
  }
};
