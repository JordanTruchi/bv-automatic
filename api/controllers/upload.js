const storage = require('../../gCloudConfig/gCloudConfig');
const DEFAULT_BUCKET_NAME = 'dopa-tech-uploads-images';
const getPublicUrl = (bucketName, fileName) =>
  `https://storage.googleapis.com/${bucketName}/${fileName}`;

module.exports = {
  upload: (req, res, next) => {
    if (!req.file) {
      return next();
    }

    const bucketName = req.body.bucketName || DEFAULT_BUCKET_NAME;
    const bucket = storage.bucket(bucketName);
    const gcsFileName = req.file.originalname;
    const file = bucket.file(gcsFileName);

    const stream = file.createWriteStream({
      metadata: {
        contentType: req.file.mimetype,
        cacheControl: 'Cache-Control: private, max-age=31536000, no-transform'
      }
    });

    stream.on('error', (err) => {
      req.file.cloudStorageError = err;
      next(err);
    });

    stream.on('finish', () => {
      req.file.cloudStorageObject = gcsFileName;

      return file.makePublic().then(() => {
        req.file.gcsUrl = getPublicUrl(bucketName, gcsFileName);
        next();
      });
    });

    stream.end(req.file.buffer);
  }
};
