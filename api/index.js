const express = require('express');
const bodyParser = require('body-parser');

// Create express instance
const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
// Require API routes
const user = require('./routes/user');
const upload = require('./routes/upload');
const sitemap = require('./routes/sitemap');
const contact = require('./routes/contact');
const { handleError } = require('./middleware/error');
// Import API Routes

app.use(user);
app.use(upload);
app.use(sitemap);
app.use(contact);
app.use(handleError);

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
};
