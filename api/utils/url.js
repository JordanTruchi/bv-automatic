module.exports = async function formatUrl (nodeEnv, port) {
  return nodeEnv === 'production'
    ? 'https://dopa-tech.fr'
    : `${process.env.HOST}:${port || 3000}/`;
};
