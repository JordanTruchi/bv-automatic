function verifyFormContactData (name, email, subject, message) {
  const regex = new RegExp(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );

  switch (true) {
    case name.length > 100:
      throw new Error('Votre nom ne peux dépasser 100 caractères');

    case !name.length:
      throw new Error('Le champ "nom" ne peux pas être vide');

    case subject.length > 200:
      throw new Error('Votre sujet ne peux pas dépasser 200 caractères');

    case !subject.length:
      throw new Error('Le champ "sujet" ne peux pas être vide');

    case message.length > 500:
      throw new Error('Votre message ne peux pas dépassser 500 caractères');

    case !message.length:
      throw new Error('Le champ "message" ne peux pas être vide');

    case !regex.test(email):
      throw new Error(
        "L'email n'est pas valide, veuillez saisir un email de type: example@gmail.com"
      );

    case !email.length:
      throw new Error('Le champ "email" ne peux pas être vide');

    default:
      console.log('Les données sont conformes !');
      return true;
  }
}

module.exports = {
  verifyFormContactData
};
