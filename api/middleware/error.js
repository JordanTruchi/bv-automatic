/**
 * @file Errors middleware
 */

/**
 * @description global http response error
 * @param {Error} err - Catched error
 * @param {Request} _req - unused
 * @param {Response} res
 * @param {Function} next
 */
const handleError = (err, _req, res, next) => {
  // Always put internal error if needed
  const statusCode = err.code || 500;
  const message = err.message || 'Erreur interne du serveur';

  const errorData = {
    status: 'KO',
    message
  };

  res.status(statusCode).json(errorData);

  errorData.code = statusCode;

  next(errorData);
};

module.exports = {
  handleError
};
