const { Router } = require('express');
const router = Router();
const contact = require('../controllers/contact');

router.post('/contact', contact.sendMail);

module.exports = router;
