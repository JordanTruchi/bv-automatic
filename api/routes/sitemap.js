const { Router } = require('express');
const router = Router();
const siteMap = require('../controllers/siteMap');

router.get('/sitemap/dynamic', siteMap.list);

module.exports = router;
