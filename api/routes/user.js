const { Router } = require('express');
const router = Router();
const user = require('../controllers/user');

const database = require('../utils/database');
const db = database.conn();

router.post('/user/login', user.login);
router.get('/user/verify', user.verify);
router.post('/user', user.create);
router.get('/template', (req, res) => {
    const sql = 'select * from `Roles`';
    try {
      db.query(sql, [], (results) => {
        console.log(results)
        return res.status(200).json(results)
      })
    } catch(err){
      console.log(err)
      next(err)
    }
});
module.exports = router;
