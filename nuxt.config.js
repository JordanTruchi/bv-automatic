const getAppRoutes = require('./utils/sitemap.js');
const formatUrl = require('./api/utils/url.js');

const {
  FRONT_PORT,
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USER,
  DATABASE_PWD,
  DATABASE_NAME,
  HOST
} = process.env;

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  server: {
    port: FRONT_PORT
  },
  env: {
    PORT: FRONT_PORT,
    HOST: HOST,
    DATABASE_HOST,
    DATABASE_PORT,
    DATABASE_USER,
    DATABASE_PWD,
    DATABASE_NAME
  },
  head: {
    htmlAttrs: {
      lang: 'fr-FR'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // opengraph
      {
        hid: 'og:type',
        property: 'og:type',
        name: 'og:type',
        content: 'website'
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        name: 'og:site_name',
        content: 'Dopa Tech'
      },
      {
        hid: 'og:locale',
        property: 'og:locale',
        name: 'og:locale',
        content: 'fr'
      },
      {
        hid: 'twitter:creator',
        property: 'twitter:creator',
        name: 'twitter:creator',
        content: '@DopaTech'
      },
      {
        hid: 'twitter:site',
        property: 'twitter:site',
        name: 'twitter:site',
        content: '@DopaTech'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.png' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {},
  /*
   ** Global CSS
   */
  css: ['~/assets/scss/grid.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/apiFront/init.js',
    { src: '~plugins/ga.js', mode: 'client' },
    '~/plugins/jsonLd.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxtjs/eslint-module'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/sitemap',
    [
      '@nuxtjs/component-cache',
      {
        max: 10000,
        maxAge: 1000 * 60 * 60
      }
    ],
    [
      'nuxt-lazy-load',
      {
        // These are the default values
        images: true,
        videos: false,
        audios: false,
        iframes: false,
        native: false,
        polyfill: true,
        directiveOnly: false,

        // Default image must be in the static folder
        defaultImage: '/logo/logo.png',

        // To remove class set value to false
        loadingClass: false,
        loadedClass: false,
        appendClass: false,

        observerConfig: {
          // See IntersectionObserver documentation
        }
      }
    ]
  ],
  sitemap: {
    hostname: formatUrl(process.env.NODE_ENV, process.env.PORT).url,
    gzip: true,
    lastmod: new Date(),
    sitemaps: [
      {
        path: '/sitemap-static.xml',
        exclude: ['/aponim/*', '/plan-du-site', '/mentions-legales'],
        gzip: true
      },
      {
        path: '/sitemap-dynamic.xml',
        routes: () => {
          return getAppRoutes();
        },
        exclude: ['/**']
      }
    ]
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: true,
    postcss: {
      /* plugins: {
        // Disable `postcss-url`
        'postcss-url': false,
        // Add some plugins
        'postcss-nested': {},
        'postcss-responsive-type': {},
        'postcss-hexrgba': {}
      }, */
      preset: {
        autoprefixer: {
          grid: true,
          flexbox: true
        }
      }
    },
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  },
  serverMiddleware: [
    // API middleware
    { path: '/api', handler: '~/api/index.js' }
    // '~/redirect301.js'
  ],
  render: {
    static: {
      maxAge: 36000
    }
  },
  router: {
    middleware: ['auth']
  }
};
