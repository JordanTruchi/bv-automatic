import api from '~/apiFront/contactRoute';

/* export const state = () => ({
  mail: {}
});
export const mutations = {
  set_mail (state, data) {
    state.articles = data;
  },
  reset_mail (state) {
    state.mail = null;
  },
}; */
export const actions = {
  sendMail ({ commit }, data) {
    return api.contact
      .sendMail(data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
};

/* export const getters = {
  getMail: (state) => {
    return state.mail;
  }
}; */
