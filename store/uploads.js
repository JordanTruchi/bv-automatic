import api from '~/apiFront/uploadRoute';

export const actions = {
  upload ({ commit }, data) {
    return api.upload
      .upload(data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
};
