
import { dataCartTest } from '~/data/dataCartTest.js';

export const state = () => ({
  cart: {
    totalPrice: 0.00,
    items: []
  }
});
export const mutations = {
  set_cart (store, data) {
    store.cart = data;
  },
  reset_cart (store) {
    store.cart = null;
  }
};
export const actions = {
  fetch ({ commit }) {
    commit('set_cart', dataCartTest);
    return dataCartTest;
  }
};

export const getters = {
  list: (state) => {
    return state.cart;
  }
};
