import api from '~/apiFront/siteMapRoute';
export const state = () => ({
  dynamicsRoute: []
});
export const mutations = {
  set_dynamics (state, data) {
    state.dynamicsRoute = data;
  }
};
export const actions = {
  list ({ commit }) {
    return api.sitemap
      .get()
      .then((response) => {
        commit('set_dynamics', response.data);
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
};
export const getters = {
  list: (state) => {
    return state.dynamicsRoute;
  }
};
