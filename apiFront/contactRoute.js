import axios from 'axios';
export default {
  contact: {
    sendMail: (data) => axios.post('/api/contact', data)
  }
};
