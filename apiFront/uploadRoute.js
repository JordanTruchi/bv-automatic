import axios from 'axios';
export default {
  upload: {
    upload: (data) => axios.post('/api/upload', data)
  }
};
