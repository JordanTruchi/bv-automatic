import axios from 'axios';
export default {
  auth: {
    me: () => axios.get('/api/user/verify'),
    login: (data) => axios.post('/api/user/login', data),
    create: (data) => axios.post('/api/user', data)
  }
};
