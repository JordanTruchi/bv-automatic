import axios from 'axios';
export default {
  sitemap: {
    get: () => axios.get('/api/sitemap/dynamic')
  }
};
